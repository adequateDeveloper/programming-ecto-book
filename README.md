# Book Programming Ecto
[![pipeline status](https://gitlab.com/adequateDeveloper/programming-ecto-book/badges/master/pipeline.svg)](https://gitlab.com/adequateDeveloper/programming-ecto-book/commits/master)

Following the book ["Programming Ecto"](https://pragprog.com/book/wmecto/programming-ecto) by authors Darin Wilson and Eric Meadows-Jönsson